/*    */ package com.interview.solution.Utils;
/*    */ 
/*    */ import java.sql.Timestamp;
/*    */ import java.text.SimpleDateFormat;
/*    */ import org.springframework.stereotype.Component;
/*    */ 
/*    */ @Component
/*    */ public class CustomUtil
/*    */ {
/*    */   public static String getFormattedDateWithoutTime(Timestamp timestamp)
/*    */   {
/* 12 */     String formattedDate = "";
/* 13 */     SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
/* 14 */     if (timestamp != null) {
/* 15 */       formattedDate = formatter.format(timestamp);
/*    */     }
/*    */ 
/* 18 */     return formattedDate;
/*    */   }
/*    */ }

/* Location:           C:\Users\puden\Desktop\solution\target\SNAPSHOT.jar
 * Qualified Name:     com.interview.solution.Utils.CustomUtil
 * JD-Core Version:    0.6.2
 */