/*    */ package com.interview.solution.Utils;
/*    */ 
/*    */ import com.interview.solution.Enums.Error;
/*    */ import com.interview.solution.Exception.ParameterNotSuppliedException;
/*    */ import java.util.Optional;
/*    */ 
/*    */ public class StringUtils
/*    */ {
/*    */   public static String capitaliseFirstLetter(String word)
/*    */   {
/* 10 */     String modifieldword = "";
/* 11 */     if (Optional.ofNullable(word).isPresent()) {
/* 12 */       String[] spiltedWord = word.split("\\s");
/* 13 */       for (int i = 0; i < spiltedWord.length; i++)
/*    */       {
/* 16 */         modifieldword = modifieldword + (spiltedWord.length == 1 ? 
/* 15 */           spiltedWord[0].substring(0, 1).toUpperCase() + spiltedWord[0].substring(1).toLowerCase() : 
/* 17 */           new StringBuilder().append(
/* 17 */           spiltedWord[i].substring(
/* 17 */           0, 1).toUpperCase()).append(spiltedWord[i].substring(1).toLowerCase()).append(" ").toString());
/*    */       }
/*    */ 
/*    */     }
/*    */     else
/*    */     {
/* 22 */       throw new ParameterNotSuppliedException(Error.WRONG_INPUT.value);
/*    */     }
/* 24 */     return modifieldword;
/*    */   }
/*    */ 
/*    */   public static String getFullName(String name1, String name2) {
/* 28 */     return capitaliseFirstLetter(name1) + " " + capitaliseFirstLetter(name2);
/*    */   }
/*    */ }

/* Location:           C:\Users\puden\Desktop\solution\target\SNAPSHOT.jar
 * Qualified Name:     com.interview.solution.Utils.StringUtils
 * JD-Core Version:    0.6.2
 */