/*    */ package com.interview.solution;
/*    */ 
/*    */ import org.springframework.boot.SpringApplication;
/*    */ import org.springframework.boot.autoconfigure.SpringBootApplication;
/*    */ 
/*    */ @SpringBootApplication
/*    */ public class SolutionApplication
/*    */ {
/*    */   public static void main(String[] args)
/*    */   {
/* 10 */     SpringApplication.run(SolutionApplication.class, args);
/*    */   }
/*    */ }

/* Location:           C:\Users\puden\Desktop\solution\target\SNAPSHOT.jar
 * Qualified Name:     com.interview.solution.SolutionApplication
 * JD-Core Version:    0.6.2
 */