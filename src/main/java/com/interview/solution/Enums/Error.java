     package com.interview.solution.Enums;
     
     public enum Error
     {
      RECORD_NOT_FOUND("No record found!"), 
      RECORD_ALREADY_EXIST("The record you are trying to add already exist!"), 
      UPDATE_UNAVAILABLE("No record found for your request!Nothing to update!"), 
      ALL_FIELDS_REQUIRED("All fields are required!"), 
      WRONG_INPUT("Something is not right!Probably you entered a wrong input or did not supplied any"), 
      NOTHING_TO_UPDATE("Nothing to update!No record found");
     
       public String value;
     
     private Error(String value) { this.value = value; }
     
     }

