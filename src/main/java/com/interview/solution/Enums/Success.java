     package com.interview.solution.Enums;
     
     public enum Success
     {
      RECORD_UPDATED("Record successfully updated!"), 
      RECORD_ADDED("Record successfully added!");
     
       public String value;
     
       private Success(String value)
       {
       this.value = value;
       }
     }

