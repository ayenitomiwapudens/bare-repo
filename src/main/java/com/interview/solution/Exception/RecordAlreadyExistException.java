     package com.interview.solution.Exception;
     
     public class RecordAlreadyExistException extends RuntimeException
     {
       private static final long serialVersionUID = 1L;
       private String message;
     
       public RecordAlreadyExistException()
       {
       }
     
       public RecordAlreadyExistException(String message)
       {
        this.message = message;
       }
       public String getMessage() {
       return this.message;
       }
     }

