     package com.interview.solution.Exception;

     import com.interview.solution.Enums.Error;
     import com.interview.solution.pojo.ApplicationResponse;
     import java.time.LocalDateTime;
     import org.slf4j.Logger;
     import org.slf4j.LoggerFactory;
     import org.springframework.http.HttpStatus;
     import org.springframework.http.ResponseEntity;
     import org.springframework.web.bind.annotation.ControllerAdvice;
     import org.springframework.web.bind.annotation.ExceptionHandler;
@ControllerAdvice
public class GenericExceptionHandler {
	final static Logger logger = LoggerFactory.getLogger(GenericExceptionHandler.class);
	@ExceptionHandler
	public ResponseEntity<ApplicationResponse> handleException(RecordNotFoundException e) {
		ApplicationResponse response = new ApplicationResponse();
		response.setMessage(e.getMessage());
		response.setTimestamp(LocalDateTime.now());
		response.setStatus(HttpStatus.NOT_FOUND.value());
		return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
	}
	@ExceptionHandler
	public void handleException(NullPointerException e) {
	e.printStackTrace();
	}
	@ExceptionHandler
	public ResponseEntity<ApplicationResponse> handleException(RecordAlreadyExistException e) {
		ApplicationResponse response = new ApplicationResponse();
		response.setMessage(e.getMessage());
		response.setTimestamp(LocalDateTime.now());
		response.setStatus(HttpStatus.FOUND.value());
		return new ResponseEntity<>(response, HttpStatus.FOUND);
	}

	@ExceptionHandler
	public ResponseEntity<ApplicationResponse> handleException(Exception e) {
		ApplicationResponse response = new ApplicationResponse();
		response.setMessage(Error.WRONG_INPUT.value);
		response.setTimestamp(LocalDateTime.now());
		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler
	public ResponseEntity<ApplicationResponse> handleException(AllfieldRequiredException e) {
		ApplicationResponse response = new ApplicationResponse();
		response.setMessage(e.getMessage());
		response.setTimestamp(LocalDateTime.now());
		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);

	}
}
