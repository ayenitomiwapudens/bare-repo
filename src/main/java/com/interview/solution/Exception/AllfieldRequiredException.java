     package com.interview.solution.Exception;
     
     public class AllfieldRequiredException extends RuntimeException
     {
       private static final long serialVersionUID = 1L;
       private String message;
     
       public AllfieldRequiredException()
       {
       }
     
       public AllfieldRequiredException(String message)
       {
       this.message = message;
       }
     
       public String getMessage() {
       return this.message;
       }
     }

