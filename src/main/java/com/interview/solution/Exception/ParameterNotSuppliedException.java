     package com.interview.solution.Exception;
     
     public class ParameterNotSuppliedException extends IllegalArgumentException
     {
       private static final long serialVersionUID = 1L;
       private String message;
     
       public ParameterNotSuppliedException()
       {
       }
     
       public ParameterNotSuppliedException(String message)
       {
        this.message = message;
       }
       public String getMessage() {
       return this.message;
       }
     }

 