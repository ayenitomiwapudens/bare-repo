     package com.interview.solution.Exception;
     
     public class RecordNotFoundException extends RuntimeException
     {
       private static final long serialVersionUID = 1L;
       private String message;
     
       public RecordNotFoundException()
       {
       }
     
       public RecordNotFoundException(String message)
       {
       this.message = message;
       }
     
       public String getMessage() {
       return this.message;
       }
     
       public void setMessage(String message) {
       this.message = message;
       }
     }

 