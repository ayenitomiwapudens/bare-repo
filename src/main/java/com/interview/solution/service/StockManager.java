package com.interview.solution.service;

import com.interview.solution.Entity.Stock;
import java.util.Collection;

public abstract interface StockManager
{
  public abstract Collection<Stock> getAllStocks();

  public abstract Stock getStock(int paramInt);

  public abstract Stock getStockByHql(String paramString);

  public abstract <T> void UpdateOrSaveStock(Stock paramStock);
}

