     package com.interview.solution.service;
     
     import com.interview.solution.Dao.GenericDao;
     import com.interview.solution.Entity.Stock;
     import java.util.Collection;
     import java.util.List;
     import org.slf4j.Logger;
     import org.slf4j.LoggerFactory;
     import org.springframework.beans.factory.annotation.Autowired;
     import org.springframework.stereotype.Service;
     import org.springframework.transaction.annotation.Transactional;
     
     @Service
     public class StockManagerImpl
       implements StockManager
     {
     
       @Autowired
       private GenericDao stockDAO;
     static final Logger logger = LoggerFactory.getLogger(StockManager.class);
     
       @SuppressWarnings("unchecked")
	public Collection<Stock> getAllStocks()
       {
       List <Stock>stocks = (List<Stock>) stockDAO.getAllRecords(Stock.class);
       return stocks;
       }
     
       @Transactional
       public Stock getStock(int id)
       {
       return (Stock)this.stockDAO.getRecordByHql("select s from Stock s where s.id='" + id + "'");
       }
     
       @Transactional
       public Stock getStockByHql(String query) {
       return (Stock)this.stockDAO.getRecordByHql(query);
       }
     
       @Transactional
       public <T> void UpdateOrSaveStock(Stock stock)
       {
       this.stockDAO.UpdateOrSave(stock);
       }
     }

