     package com.interview.solution.pojo;
     
     public class StockPojo
     {
       private Integer id;
       private String name;
       private String currentPrice;
       private String createDate;
       private String updateDate;
     
       public Integer getId()
       {
       return this.id;
       }
       public void setId(Integer id) {
       this.id = id;
       }
       public String getName() {
       return this.name;
       }
       public void setName(String name) {
       this.name = name;
       }
       public String getCurrentPrice() {
       return this.currentPrice;
       }
       public void setCurrentPrice(String currentPrice) {
       this.currentPrice = currentPrice;
       }
       public String getCreateDate() {
       return this.createDate;
       }
       public void setCreateDate(String createDate) {
       this.createDate = createDate;
       }
       public String getUpdateDate() {
       return this.updateDate;
       }
       public void setUpdateDate(String updateDate) {
       this.updateDate = updateDate;
       }
     }

