package com.interview.solution.SE;

import java.util.Arrays;

public class Simplejava {
	private static int minimum;
	private static int maximum;
	private static int[] returnedArray;

	public static int findMinimumElement(int[] a) {
		minimum = a[0];
		for (int i = 0; i < a.length; i++) {
			if (a[i] < minimum) {
				minimum = a[i];
			}
		}
		return minimum;
	}

	public static int findMaximumElement(int[] a) {
		maximum = a[0];
		for (int i = 0; i < a.length; i++) {
			if (a[i] > maximum) {
				maximum = a[i];
			}
		}
		return maximum;
	}

	public static void simpleJava(int[] a) {
		minimum = findMinimumElement(a);
		maximum = findMaximumElement(a);
		returnedArray = new int[maximum];

		for (int i = minimum, j = 0; i <= maximum; i++, j++) {
			returnedArray[j] = i;
		}

		int[] missingElements = findMissingElements(returnedArray, a, returnedArray.length, a.length);
		int d = findMinimumElement(missingElements);
		System.out.println(d);
	}

	static int[] findMissingElements(int[] generatedArray, int[] originalArray, int generatedArrayLength,
			int originalArrayLength) {
		int j;
		String output = "";
		int[] array = null;
		for (int i = 0; i < generatedArrayLength; i++) {

			for (j = 0; j < originalArrayLength; j++) {
				if (generatedArray[i] == originalArray[j])
					break;
			}

			if (j == originalArrayLength) {
				output += generatedArray[i] + " ";
				String[] missingElements = output.split(" ");
				array = Arrays.stream(missingElements).mapToInt(Integer::parseInt).toArray();
			}

		}
		return array;
	}

	public static void main(String[] args) {
		simpleJava(new int[] { 3, 4, 2, 6, 1 });
	}

}
