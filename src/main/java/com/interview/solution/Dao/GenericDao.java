package com.interview.solution.Dao;

import java.util.Collection;

public interface GenericDao {

	public <T> T getRecordById(T t, int id);

	public <T> T getRecordByHql(String queryString);

	public Collection<?> getAllRecordsByHQL(String queryString);

	public <T> void UpdateOrSave(T object);

	@SuppressWarnings("rawtypes")
	public Collection getAllRecords(Class clazz);
	
}