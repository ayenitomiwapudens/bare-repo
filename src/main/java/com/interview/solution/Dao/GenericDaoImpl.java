package com.interview.solution.Dao;

import java.util.Collection;
import javax.persistence.EntityManager;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
//import com.rapidlink.webservices.webservices.model.PortalUser;
//import com.rapidlink.webservices.webservices.service.Service;

@Repository
@org.springframework.stereotype.Service
public class GenericDaoImpl implements GenericDao {
	final static Logger logger = LoggerFactory.getLogger(GenericDaoImpl.class);

	@Autowired
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public <T> T getRecordById(T object, int id) {
		Session currentSession = entityManager.unwrap(Session.class);
		return currentSession.get((Class<T>) object, id);

	}
	@SuppressWarnings("rawtypes")
	public Collection getAllRecords(Class clazz)
	  {
	    String queryString = "SELECT u FROM " + clazz.getName() + " u";
	    Session currentSession = entityManager.unwrap(Session.class);
		Query<?> query = currentSession.createQuery(queryString);
	    Collection returnCol = query.getResultList();
	    return returnCol;
	  }

	public Collection<?> getAllRecordsByHQL(String queryString) {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<?> query = currentSession.createQuery(queryString);

		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T getRecordByHql(String queryString) {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<?> query = currentSession.createQuery(queryString);

		return (T) query.uniqueResult();
	}

	@Override
	public <T> void UpdateOrSave(T object) {
		Session currentSession = entityManager.unwrap(Session.class);
		currentSession.merge(object);

	}

	

//	@Override
//	public <T> void UpdateRecordByHql(String queryString) {
//		Session currentSession = entityManager.unwrap(Session.class);
//		Query query = currentSession.createQuery(queryString);
//		query;
//		
//	}
}
