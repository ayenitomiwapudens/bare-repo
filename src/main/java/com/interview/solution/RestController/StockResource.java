package com.interview.solution.RestController;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.interview.solution.Entity.Stock;
import com.interview.solution.Enums.Error;
import com.interview.solution.Enums.Success;
import com.interview.solution.Exception.AllfieldRequiredException;
import com.interview.solution.Exception.RecordAlreadyExistException;
import com.interview.solution.Helper.StockHelper;
import com.interview.solution.Utils.CustomUtil;
import com.interview.solution.Utils.StringUtils;
import com.interview.solution.pojo.ApplicationResponse;
import com.interview.solution.pojo.StockPojo;
import com.interview.solution.service.StockManager;

@RestController
@RequestMapping({ "/api/stocks" })
public class StockResource {
	static final Logger logger = LoggerFactory.getLogger(StockResource.class);

	@Autowired
	StockHelper stockHelper;

	@Autowired
	StockManager stockManager;

	@Autowired
	CustomUtil customUtil;

	@PostMapping
	@ResponseBody
	public ApplicationResponse addStock(@RequestBody Stock stock, BindingResult result) {
		ApplicationResponse response = new ApplicationResponse();
		try {
			if ((stock.getName() == null) || (stock.getCurrentPrice() < 0L)) {
				response.setSuccessResponse(Error.ALL_FIELDS_REQUIRED.value);
				response.setTimestamp(LocalDateTime.now());
				response.setStatus(HttpStatus.BAD_REQUEST.value());
				throw new AllfieldRequiredException(Error.ALL_FIELDS_REQUIRED.value);
			}

			Optional<String> stockOpt = Optional.ofNullable(stock.getName());
			if (stockOpt.isPresent()) {
				if (this.stockHelper
						.isStockExist(StringUtils.capitaliseFirstLetter(((String) stockOpt.get()).toString()))) {
					response.setMessage(Error.RECORD_ALREADY_EXIST.value);
					response.setTimestamp(LocalDateTime.now());
					response.setStatus(HttpStatus.BAD_REQUEST.value());
					throw new RecordAlreadyExistException(Error.RECORD_ALREADY_EXIST.value);
				}
				if (stock != null) {
					stock.setName(StringUtils.capitaliseFirstLetter(stock.getName()));
					logger.info("<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>" + stock.getName());
					this.stockHelper.addStock(stock);
					response.setSuccessResponse(Success.RECORD_ADDED.value);
					response.setTimestamp(LocalDateTime.now());
					response.setStatus(HttpStatus.OK.value());
					logger.info("<<<<<<<<<<<<<<<<<<<<STOCK ADDED>>>>>>>>>>>>>>>>>>>>>>>>>>");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	@GetMapping
	@ResponseBody
	public List<StockPojo> getAllStocks() {
		List<StockPojo> items = new ArrayList<>();
		int count = 1;
		List<Stock> stocks = null;
		try {
			stocks = (List<Stock>) stockManager.getAllStocks();
			if (stocks != null)
				for (Stock s : stocks) {
					StockPojo pojo = new StockPojo();
					pojo.setId(count);
					pojo.setName(s.getName());
					pojo.setCurrentPrice(String.format("₦ %s", s.getCurrentPrice()));
					pojo.setCreateDate((CustomUtil.getFormattedDateWithoutTime(s.getCreateDate())));
					if (s.getUpdateDate() != null) {
						pojo.setUpdateDate((CustomUtil.getFormattedDateWithoutTime(s.getUpdateDate())));
					}
					count++;
					items.add(pojo);
				}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return items;
	}

	@GetMapping({ "/{id}" })
	@ResponseBody
	public Stock getStockById(@PathVariable("id") int id) {
		ApplicationResponse response = new ApplicationResponse();
		Stock stock = null;
		try {
			Optional<Integer> idOpt = Optional.ofNullable(Integer.valueOf(id));
			if (idOpt.isPresent()) {
				if (this.stockManager.getStock(id) == null) {
					response.setMessage(Error.RECORD_NOT_FOUND.value);
					response.setStatus(HttpStatus.BAD_REQUEST.value());
					response.setTimestamp(LocalDateTime.now());
				} else {
					stock = this.stockManager.getStock(id);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return stock;
	}

	@PutMapping
	@ResponseBody
	public ApplicationResponse updateStock(@RequestBody Stock stock, BindingResult result) {
		ApplicationResponse response = new ApplicationResponse();
		try {
			if ((stock != null) && (stock.getId() > 0))
				if (stockManager.getStock(stock.getId()) == null) {
					response.setMessage(Error.NOTHING_TO_UPDATE.value);
					response.setStatus(HttpStatus.BAD_REQUEST.value());
					response.setTimestamp(LocalDateTime.now());
				} else {
					logger.info("<<<<<<<<<<<<<<<<<<<<ABOUT TO UPDATE STOCK>>>>>>>>>>>>>>>>>>>>>>>>>>");
					stock.setUpdateDate(new Timestamp(System.currentTimeMillis()));
					this.stockManager.UpdateOrSaveStock(stock);
					response.setSuccessResponse(Success.RECORD_UPDATED.value);
					response.setTimestamp(LocalDateTime.now());
					response.setStatus(HttpStatus.OK.value());
					logger.info("<<<<<<<<<<<<<<<<<<<<STOCK UPDATED>>>>>>>>>>>>>>>>>>>>>>>>>>");
				}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}
}
