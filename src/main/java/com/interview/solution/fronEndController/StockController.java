     package com.interview.solution.fronEndController;
     
     import org.springframework.stereotype.Controller;
     import org.springframework.web.bind.annotation.GetMapping;
     import org.springframework.web.bind.annotation.RequestMapping;
     
     @Controller
     @RequestMapping({"/"})
     public class StockController
     {
       @GetMapping
       public String stockList()
       {
       return "stockList";
       }
     }

