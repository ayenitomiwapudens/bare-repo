     package com.interview.solution.Helper;
     
     import com.interview.solution.Entity.Stock;
     import com.interview.solution.Utils.StringUtils;
     import com.interview.solution.service.StockManager;
     import java.sql.Timestamp;
     import org.slf4j.Logger;
     import org.slf4j.LoggerFactory;
     import org.springframework.beans.factory.annotation.Autowired;
     import org.springframework.stereotype.Component;
     
     @Component
     public class StockHelper
     {
     
       @Autowired
       StockManager stockManager;
     static final Logger logger = LoggerFactory.getLogger(StockHelper.class);
     
       public void addStock(Stock stock)
       {
       stock.setId(Integer.valueOf(0));
       if (stock.getName() != null) {
         StringUtils.capitaliseFirstLetter(stock.getName());
         }
     
       stock.setCreateDate(new Timestamp(System.currentTimeMillis()));
       this.stockManager.UpdateOrSaveStock(stock);
       }
     
       public boolean isStockExist(String name) {
       Stock stock = this.stockManager.getStockByHql("select s from Stock s where s.name='" + name + "'");
       logger.info(stock + "This is the stock");
       if (stock != null) {
         return Boolean.TRUE.booleanValue();
         }
     
       return Boolean.FALSE.booleanValue();
       }
     }

