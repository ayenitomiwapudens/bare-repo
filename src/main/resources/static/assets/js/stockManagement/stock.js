$('document').ready(function() {
	getAllStocks();


});



function getAllStocks() {

	var url = "/api/stocks";
	
	$
			.ajax({
				method : 'GET',
				url : url,
				dataType : 'json',
				success : function(data) {
					console.log(data);
					$('#stock_list').dataTable().fnDestroy();
					$('#stock_list')
							.dataTable(
									{
										"processing" : true,
										"serverSide" : false,
										"ordering" : false,
										"bFilter" : true,
										"pagingType" : "full_numbers",

										data : data,
										columns : [
												{
													'data' : 'id'
												},
												{
													'data' : 'name'
												},
												{
													'data' : 'currentPrice'
												},
												{
													'data' : 'createDate'
												},
												{
													'data' : 'updateDate'
												}
												
												]
									});
					
				}
			});
	}
